# Backend
This folder contains yaml files for AWS deployment.

### Prerequisites
- AWS CLI
- AWS Account
- AWS Dynamo DB (but can create it too in YAML files)

### Instruction
- sam build
- sam deploy

### For testing the Graph QL API
- Please message me directly so I can give you the URL and API KEY