import './App.css';
import NavBar from './components/navbar';
import { ThemeProvider } from '@emotion/react';
import { CssBaseline } from '@mui/material';
import theme from './theme'
import TodoList from './components/todos/TodoList';

const App = () => (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <div>
        <NavBar/>
        <TodoList/>
      </div>
    </ThemeProvider>
  )

export default App;