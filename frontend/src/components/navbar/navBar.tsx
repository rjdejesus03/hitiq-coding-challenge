import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';


const NavBar: React.FC = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
          </IconButton>
          <Typography variant="h5"  component="div" sx={{ flexGrow: 1 }}>
            <b> HitIQ Coding Challenge</b>
          </Typography>
          <Typography variant="h6" component="div" sx={{ flexGrow: 0 }}>
            <b>RJ DE JESUS</b>
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default NavBar