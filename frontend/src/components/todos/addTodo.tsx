import React, { Dispatch, useState } from "react";
import { connect } from "react-redux";
import type { RootState, AppDispatch } from "../../stores/stores";
import { processTodo } from "../../stores/todos/todos-action";
import { TextField, Paper, Button, Grid } from "@mui/material";
import TodoListItem from "./TodoListItem";

interface Props {
  onCreatePressed: any;
}

const AddTodo: React.FC<Props> = ({ onCreatePressed }) => {
  
  const initialValues = {
    name: "",
    description: "",
    priority: 1,
  };

  const [allValues, setAllValues] = useState(initialValues);

  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAllValues({ ...allValues, [e.target.name]: e.target.value });
  };

  let todo: Todo = {
    name: ""
  }

  const onClick = () => {
    if (allValues.priority > 0 && !isNaN(allValues.priority)) {
      todo = allValues;
      onCreatePressed(todo);
      setAllValues(initialValues);
      processTodo(todo);
    } else {
      alert("Pease enter valid number!");
    }
  };

  return (
    <Paper
      elevation={3}
      style={{
        margin: "auto",
        marginTop: 15,
        padding: 16,
        width: "100%",
        maxWidth: 800,
      }}
    >
      <Grid container spacing={2} alignItems="center">
        <Grid xs={12} md={12} item style={{ paddingRight: 1 }}>
          <TextField
            label="Todo"
            fullWidth
            name="name"
            onChange={changeHandler}
          />
        </Grid>
        <Grid xs={10} md={10} item style={{ paddingRight: 1 }}>
          <TextField
            label="Description"
            fullWidth
            name="description"
            onChange={changeHandler}
          />
        </Grid>
        <Grid xs={2} md={2} item style={{ paddingRight: 1 }}>
          <TextField
            label="Priority"
            type="number"
            InputProps={{
              inputProps: { min: 1 },
            }}
            fullWidth
            name="priority"
            value={allValues.priority}
            onChange={changeHandler}
          />
        </Grid>
        <Grid xs={12} md={12} item>
          <Button
            color="primary"
            variant="contained"
            fullWidth
            onClick={() => onClick()}
          >
            SUBMIT
          </Button>
        </Grid>
      </Grid>
    </Paper>
  );
};

const mapStateToProps = (state: RootState) => ({
  todos: state.todos,
});
const mapDispatchToProps = (dispatch: any) => ({
  onCreatePressed: (todo: Todo) => dispatch(processTodo(todo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo);
