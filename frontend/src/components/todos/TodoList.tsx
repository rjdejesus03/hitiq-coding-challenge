import React, { useEffect } from "react";
import { connect } from "react-redux";
import AddTodo from "./AddTodo";
import TodoListItem from "./TodoListItem";
import { AppDispatch, RootState } from "../../stores/stores";
import {loadTodos} from "../../stores/todos/todos-action";
import { Box, CircularProgress } from "@mui/material";

interface Props {
  isLoading: boolean,
  todos?: Todo[],
  startLoadingTodos: any
}

const TodoList: React.FC<Props> = ({ todos = [], isLoading, startLoadingTodos }) => {

    useEffect(() => {
        startLoadingTodos();
    }, []);
    
    const loadingMessage = (
    <Box sx={{ display: 'flex', alignContent: 'center', justifyContent: 'center', margin: 'auto', mt: 25, padding: '20', width: '20%'}}>
        <CircularProgress size={199}/>
      </Box>
    )

    const content = (
    <div className="list-wrapper">
      <AddTodo />
      {todos.map((todo) => (
        <TodoListItem todo={todo}  />
      ))}
    </div>
    )
    return isLoading? loadingMessage : content;
};

const mapStateToProps = (state: RootState) => ({
  isLoading: state.isLoading,
  todos: state.todos,
});

const mapDispatchToProps = (dispatch: any) => ({
    startLoadingTodos: () => dispatch(loadTodos()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
