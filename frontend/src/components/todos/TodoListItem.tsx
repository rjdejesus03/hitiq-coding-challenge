import React, { memo } from "react";
import {
  Grid,
  Typography,
  List,
  ListItem,
  IconButton,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Paper,
} from "@mui/material";
import { Delete, PriorityHigh, Note } from "@mui/icons-material";

interface Props {
  todo: Todo;
}

const TodoListItem: React.FC<Props> = ({ todo }) => {
  return (
    <Paper
      elevation={3}
      style={{
        margin: "auto",
        marginTop: 15,
        padding: 15,
        width: "100%",
        maxWidth: 800,
      }}
    >
      <Grid item xs={12} md={6}>
        <Grid container direction="row" alignItems="center">
          <Grid item>
            <Note fontSize="large" color="primary" />
          </Grid>
          <Grid item>
            <Typography sx={{ mb: 1, ml: 1 }} variant="h6" component="div">
              {todo.id}
            </Typography>
          </Grid>
        </Grid>

        <List dense={true}>
          <ListItem
            secondaryAction={
              <IconButton edge="end" aria-label="delete">
                <Delete />
              </IconButton>
            }
          >
            <ListItemAvatar>
              <PriorityHigh color="success" />
            </ListItemAvatar>
            <ListItemText
              primary={todo.name}
              secondary={todo.description}
            />
          </ListItem>
        </List>
      </Grid>
    </Paper>
  );
};
export default TodoListItem;
