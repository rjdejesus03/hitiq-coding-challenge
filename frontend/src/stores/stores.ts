import { configureStore, combineReducers} from "@reduxjs/toolkit";
import { reducers } from "./root-reducers";
import logger from 'redux-logger'

const rootReducer = combineReducers(reducers);

export const store = configureStore({
    reducer: rootReducer,
    devTools: process.env.NODE_ENV !== 'production',
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({serializableCheck: false}).concat(logger),
    
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch