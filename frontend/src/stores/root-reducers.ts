import { isLoading, todos } from "./todos/todos-reducer";

export const reducers = {
    isLoading,
    todos,
}