import { CREATE_TODO, CREATE_TODO_SUCCESS, CREATE_TODO_ERROR, LOAD_TODOS_IN_PROGRESS, LOAD_TODOS_SUCCESS, LOAD_TODOS_FAILURE } from './todos-action';

export interface loadTodosInProgress {
    type: typeof LOAD_TODOS_IN_PROGRESS;
}

export interface loadTodosSuccess {
    type: typeof LOAD_TODOS_SUCCESS;
    payload: Todo[]
}

export interface loadTodosFailure {
    type: typeof LOAD_TODOS_FAILURE;
}

export interface createTodo {
    type: typeof CREATE_TODO;
    payload: Todo
}

export interface createTodoSuccess {
    type: typeof CREATE_TODO_SUCCESS;
    payload: Todo
}

export interface createTodoError {
    type: typeof CREATE_TODO_ERROR;
    payload: Todo
}



export type TodoActionTypes = loadTodosInProgress | loadTodosSuccess | loadTodosFailure | createTodo | createTodoSuccess | createTodoError;