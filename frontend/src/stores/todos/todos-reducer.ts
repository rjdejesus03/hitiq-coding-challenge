import { CREATE_TODO, CREATE_TODO_SUCCESS, LOAD_TODOS_IN_PROGRESS, LOAD_TODOS_SUCCESS, LOAD_TODOS_FAILURE } from "./todos-action";
import { TodoActionTypes } from "./todos-types";


export const isLoading = (state = false, action: TodoActionTypes) => {
    const { type } = action;
    switch (type){
        case LOAD_TODOS_IN_PROGRESS: 
            return true;
        case LOAD_TODOS_SUCCESS: 
        case LOAD_TODOS_FAILURE: 
            return false
        default:
            return state;
    }
}


export const todos = (state: any = [], action: TodoActionTypes) => {
    switch (action.type) {
        case CREATE_TODO: {
            const todo: Todo = action.payload;
            return [
                ...state,
                todo
              ];   
        }

        case CREATE_TODO_SUCCESS: {
            return [
                ...state,
              ];   
        }

        case LOAD_TODOS_SUCCESS: {
            const todos: any = action.payload
            return [
                ...state,
                ...todos,
              ]; 
        }

        default: {
            return state;
        }
    }
}