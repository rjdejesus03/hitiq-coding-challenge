import { TodoActionTypes } from "./todos-types";
import { TodoApi } from "../../api/todos-api";
import type { RootState, AppDispatch } from '../../stores/stores'
import { v4 as uuidv4 } from "uuid";

export const LOAD_TODOS_IN_PROGRESS = 'LOAD_TODOS_IN_PROGRESS';
export const LOAD_TODOS_SUCCESS = 'LOAD_TODOS_SUCCESS';
export const LOAD_TODOS_FAILURE = 'LOAD_TODOS_FAILURE';
export const CREATE_TODO = 'CREATE_TODO';
export const CREATE_TODO_SUCCESS = 'CREATE_TODO_SUCCESS';
export const CREATE_TODO_ERROR = 'CREATE_TODO_ERROR';

export const loadTodosInProgress = () => ({
    type: LOAD_TODOS_IN_PROGRESS,
});

export const loadTodosSuccess = (todos: Todo[]) => ({
    type: LOAD_TODOS_SUCCESS,
    payload: todos,
});

export const loadTodosFailure = () => ({
    type: LOAD_TODOS_FAILURE,
});

export const loadTodos = () => async (dispatch: AppDispatch, getState: RootState) => {
    try {
        dispatch(loadTodosInProgress());
        const res = await TodoApi.getAllTodos()
        dispatch(loadTodosSuccess(res.data.data.getTodos));
    } catch (e) {
        dispatch(loadTodosFailure());
      
    }
}

export const createTodo = (todo: Todo): TodoActionTypes => ({
    type: CREATE_TODO,
    payload: todo
});

export const createTodoSuccess = (todo: Todo): TodoActionTypes => ({
    type: CREATE_TODO_SUCCESS,
    payload: todo
});

export const createTodoError = (error: any): TodoActionTypes => ({
    type: CREATE_TODO_ERROR,
    payload: error
});

 export const processTodo = (todo: Todo) => {
    return async (dispatch: AppDispatch, getState: RootState) => {
        try {
            todo.id = uuidv4();
            dispatch(createTodo(todo));
            const res = await TodoApi.createTodo(todo)
            dispatch(createTodoSuccess(res.data.data.addTodo))
        }catch (e) {
            dispatch(createTodoError(e))
        }
    }
}
