interface Todo {
    id?: String,
    name: String
    description?: String
    priority?: Number
}
