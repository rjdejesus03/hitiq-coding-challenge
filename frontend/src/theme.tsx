import { createTheme } from "@mui/material"
import { ThemeOptions } from "@mui/material";

const themeOptions: ThemeOptions = {
  palette: {
    primary: {
      main: '#84DCCF',
    },
    secondary: {
      main: '#A6D9F7',
    },
    background: {
      default: '#312F2F',
    },
    error: {
      main: '#BF98A0',
    },
  },
};

const theme = createTheme(themeOptions)

export default theme