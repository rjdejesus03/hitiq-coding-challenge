import React from 'react';
import { Provider } from 'react-redux';
import { createRoot } from 'react-dom/client';
import { store } from './stores/stores';
import App from './App';

const container = document.getElementById('root')!;

createRoot(container).render(
        <Provider store={store}>
        <App />
     </Provider>
);