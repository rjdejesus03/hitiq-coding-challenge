import { HttpClient } from "./http-client";

const API =
  "https://4nvbheplgja25nferrjhm52bza.appsync-api.ap-northeast-1.amazonaws.com/graphql";
const API_KEY = "da2-lbjpmq7ufnc6rjx7mhngmuaw2a";

const config = {
  headers: {
    "x-api-key": API_KEY,
  },
};

const createTodo = async (payload: Todo) => {
  const query = {
    query: `mutation { addTodo(todo: {description: "${payload.description}", id: "${payload.id}", name: "${payload.name}", priority: ${payload.priority}}) { id description name priority }}`,
  };
  console.log(query)
  return HttpClient.post(API, query, config);
};

const getAllTodos = async () => {
  const query = {
    "query": "query { getTodos { description name id priority }}"
  }
  console.log(query)
  return HttpClient.post(API, query, config);
};

const TodoApi = { createTodo, getAllTodos };

export { TodoApi };
