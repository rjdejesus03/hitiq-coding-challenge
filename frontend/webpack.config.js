const path = require("path");
const webpack = require('webpack');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const ReactRefreshTypeScript = require('react-refresh-typescript');

const isDevelopment = process.env.NODE_ENV !== 'production';

module.exports = {
    entry: './src/index.tsx',
    mode: isDevelopment ? 'development' : 'production',
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
    },
    devServer: { contentBase: path.join(__dirname, "src") },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ["babel-loader"],
            },
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                use: [
                    {
                      loader: require.resolve('ts-loader'),
                      options: {
                        getCustomTransformers: () => ({
                          before: [isDevelopment && ReactRefreshTypeScript()].filter(Boolean),
                        }),
                        transpileOnly: isDevelopment,
                      },
                    }
                ]
            },
            {
                test: /\.(css|scss)$/,
                use: ["style-loader", "css-loader"],
            },
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist/'),
        publicPath: '/dist',
        filename: 'bundle.js'
    },
    devServer: {
        static : {
          directory : path.join(__dirname, "public/")
        },
        port: 3000,
        devMiddleware:{
           publicPath: "https://localhost:3000/dist/",
        },
        hot: true,
      },
    plugins: [
        isDevelopment && new ReactRefreshWebpackPlugin()
    ].filter(Boolean),
}
